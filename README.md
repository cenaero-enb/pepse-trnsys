# PEPSE TRNSYS library

## Introduction

TRNSYS component library dedicated to PEPSE project

Current components

- HVAC components
- Utility
- RCModels

## Installation

### Prerequisite

- TRNSYS 18 (64 bit): The components in this library will not work with lower version. For TRNSYS 18 32 bit, the library need to be rebuilt
- Python 3.6 (any python 3.6 version for precomibled dll file, for other python 3 version the library need to be rebuilt) version 3.7 seems present bug

### Install

- Copy pepse-trnsys.dll in bin to C:\TRNSYS18\UserLib\ReleaseDLLs (replace the old version)
- Create a directory (if not exist) in C:\TRNSYS18\Studio\Proformas, e.g. PEPSE, and put all sub-folder in Proformas (HVAC, RCModels, Utility) into this directory
- That's it. PEPSE-TRNSYS library is ready to use now

## Building pepse-trnsys

### Dependencies

- python 3.6
- pybind11
- boost 1.69

### Build with Visual Studio

Visual sutio 2017 is recommended

Open pepse-trnsys.sln using visual studio 2017

## Usage

The use of compoments in this library is similar to other trnsys component.

With Type 267 is similar to Type 169 in TRNSYS STANDARD COMPONENTS LIBRARY with the addition of some adaptations for PEPSE project. The principle for using this component in a trnsys project is therefore similar to Type 169.

![error message for Type 267](assets/img/error_message_T169.png)

The detailed message is below

```
*** Fatal Error at time   :         0.000000
     Generated by Unit     : Not applicable or not available
     Generated by Type     :   267
     TRNSYS Message    105 : A TYPE was called in the TRNSYS input file but was either not linked into trndll.dll or was not found in an external dll. A dummy subroutine was called in its place. Please link the TYPE or remove it from the input file
     Reported information  :  Type169 could not be located in either the trndll.dll or in an external dll. Please relink theTRNDll.dll including this Type or make sure that an external DLL in the \UserLib\DebugDLLs and \UserLib\ReleaseDLLs folders contain the Type.
```

This is a message that usually says that the Type DLL is missing or the associated DLL is missing. It's a little confusing, but Type267 requires a Pyhton runtime environment. This error will occur if the environment is not prepared in advance before using Type169.

## Contribution

## Examples
